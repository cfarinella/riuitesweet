$(function() {
  $('.connect_button_container > a').click(function() {
    _gaq.push(['_trackSocial','facebook','like']);
  });

  //page section hash link tracking code
  $('.facebook-menu').find('a[href="#sweet-red"]').click(function() {
    _gaq.push(['_trackPageview','fb-our-wines/sweet-red']);
  });

  $('.facebook-menu').find('a[href="#sweet-white"]').click(function() {
    _gaq.push(['_trackPageview','fb-our-wines/sweet-white']);
  });

  $('.facebook-menu').find('a[href="http://locator.banfi-hq.com/WineLocator/Default.aspx?BCode=289"]').click(function() {
    _gaq.push(['_trackPageview','/store-locator']);
  });

  $('.facebook-menu').find('a[href="#free-iphone"]').click(function() {
    _gaq.push(['_trackPageview','fb-our-wines/free-iphone']);
  });

  //Additional link tracking for the FB Tab
  /*
  $('a[href="http://www.riunitesweet.com"]').click(function () {
    _gaq.push(['_trackEvent','fb-our-wines','chill-chatter','visit']);
  });*/
  $('.facebook-menu').find('a[href="#chill-chatter-content"]').click(function() {
    _gaq.push(['_trackPageview','/fb-chill-chatter']);
  });

  $('.facebook-menu').find('a[href="#"]').click(function() {
    _gaq.push(['_trackEvent','facebook','our-wines', 'link-to-timeline']);
  });

  $('a[href="https://www.facebook.com/RiuniteSweet/videos"]').click(function() {
    _gaq.push(['_trackEvent','fb-our-wines','videos','visit']);
  });

  $('a[href="http://pinterest.com/riunitesweet/"]').click(function() {
    _gaq.push(['_trackEvent','fb-our-wines','pinterest','visit']);
  });

  //Social Exit tracking
  $('.twitter2').click(function () {
    _gaq.push(['_trackSocial','fb-to-twitter','visit',]);
  });

  $('.youtube2').click(function () {
    _gaq.push(['_trackSocial','fb-to-youtube','visit',]);
  });

  $('.spotify2').click(function () {
    _gaq.push(['_trackSocial','fb-t0-pinterest','visit',]);
  });
});
