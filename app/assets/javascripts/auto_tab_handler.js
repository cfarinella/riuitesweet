function AutoTabHandler() {

	var currentField = null;
	var nextField = null;

	this.bind = function(current_field, next_field){
		currentField = $(current_field);
		nextField = $(next_field);
		
		$(currentField).keyup(function(){
			handleTab();
			checkComplete();
		});	
	}

	function handleTab() {
		if($(currentField).attr('maxlength') && 
			$(currentField).val().length == $(currentField).attr('maxlength')) {
				$(nextField).focus();
			}
	}

	function checkComplete() {
		if($('#month').val().length == $('#month').attr('maxlength')
			&& $('#day').val().length == $('#day').attr('maxlength')
			&& $('#year').val().length == $('#year').attr('maxlength')) {
				$('#age-verify-buttons').fadeIn(400);
		}
		else {
			$('#age-verify-buttons').fadeOut(400);
		}
	}
}