function FbLinkHandler() {

	var redirectLink = null;	
	$elem = null;

	this.init = function(elem, url){
		$elem = $(elem);
		redirectLink = url;
		bindActions();
	}

	function bindActions() {
		$elem.on('click', function(e){
			e.preventDefault();
			redirect();
		});
	}

	function redirect() {
		top.location.href = redirectLink;
	}
}