class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_locale

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
    @locale = I18n.locale
    params[:locale] = @locale

    # Permanent redirect to facebook.
    # if ENV['RAILS_ENV'] == "production"
    #   if I18n.locale.to_s === "en"
    #     redirect_to 'http://www.facebook.com/RiuniteSweet', :status => 301
    #   else
    #     redirect_to 'http://www.facebook.com/RiuniteSweetLatino', :status => 301
    #   end
    # end
  end

  def get_campaign_appended_path(path)

    campaign_appended_path = path
    if params[:cid] then
      campaign_appended_path = campaign_appended_path + '?cid=' + params[:cid]
    end

    return campaign_appended_path
  end
end
