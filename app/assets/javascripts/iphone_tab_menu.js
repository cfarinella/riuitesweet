IphoneTabMenu = {
	tab_menu: null,
	tab_contents: null,
	env: null,
	background_image: null,

	init: function(tab_menu, tab_contents, env) {
		this.tab_menu     = $(tab_menu);
		this.tab_contents = $(tab_contents);
		this.env		  = env;
		this.background_image = 'url(' + this.env + '/assets/layout/tab-active.png)';

		this.bindActions();
		$('#sweet-red').hide();
	},

	bindActions: function() {
		var parent = this;
		var selected;

    this.tab_menu.find('.show-tab-content').each(function() {
		$(this).click(function(e) {
			e.preventDefault();

	        parent.tab_contents.find('.show-tab-content').each(function() {
				$(this).hide();
			});

			//Possible fix for FB tab that just checks the ul class
			//and if its FB than there is no animation
			if ($(this).attr('id') == "fb-tab") {
				parent.showBoxNoAni($(this).attr('href'));
			} 
			else {
				parent.showBox($(this).attr('href'));
			}

			parent.hideBackgrounds();

			$(this).find('.caption-wrapper').addClass('caption-wrapper-active');
		});

		$(this).hover(function(e) {
			if ($(this).find('.caption-wrapper').css('background') != parent.background_image) {
      			$(this).find('.caption-wrapper').toggleClass('caption-wrapper-hover');
			}
		},
		function(e) {
			if ($(this).find('.caption-wrapper').css('background') != parent.background_image) {
      			$(this).find('.caption-wrapper').toggleClass('caption-wrapper-hover');
			}
		});
	});

    this.tab_menu.find('.not-ready').each(function() {
      $(this).hover(function(e) {
        if ($(this).find('.caption-wrapper').css('background') != parent.background_image) {
          $(this).find('.caption-wrapper').toggleClass('caption-wrapper-hover');
        }
      },
      function(e) {
        if ($(this).find('.caption-wrapper').css('background') != parent.background_image) {
          $(this).find('.caption-wrapper').toggleClass('caption-wrapper-hover');
        }
      });
    });
	},

	showBox: function(box_id) {
    $(box_id).show();
    $('html, body').animate({scrollTop: $('.content').height()}, 500);
	},

	showBoxNoAni: function(box_id) {
		$(box_id).show();
	},

	hideBackgrounds: function() {
    $('.caption-wrapper').removeClass('caption-wrapper-active');
	}
};