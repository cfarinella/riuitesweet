class CreateUseragents < ActiveRecord::Migration
  def change
    create_table :useragents do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
