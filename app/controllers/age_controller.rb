class AgeController < ApplicationController

  def index

    # handle specific content
    @content = ''
    if params[:content] == 'sweet-red-wine' || params[:content] == 'sweet-white-wine' || params[:content] == 'chill-girls-videos'
      @content = params[:content]
    else

      # handle facebook request for meta tags (allows question text
      # to be embedded in description)
      @meta_id = params[:content] || ''
      @meta_description = ''
      if @locale.to_s == "es"
        begin
          @meta_description = Question.find(@meta_id).es
          rescue ActiveRecord::RecordNotFound
        end
      else
        begin
          @meta_description = Question.find(@meta_id).en
          rescue ActiveRecord::RecordNotFound
        end
      end
    
    end

    # if already logged in just go to the questions page
    if session[:age_verified] == 1
      redirect_to "/#{@locale}/#{@content}", :status => 301
    end

  end

  def verify

    #verify date is greater than the current date - 21 years
    birth_date = Date.civil(
      params[:year].to_i,
      params[:month].to_i,
      params[:day].to_i
    )

    @content = ''
    if params[:content] == 'sweet-red-wine' || params[:content] == 'sweet-white-wine' || params[:content] == 'chill-girls-videos'
      @content = params[:content]
    end


    if birth_date.jd  <= (Date.today - 21.years).jd then
      session[:age_verified] = 1

      redirect_to self.get_campaign_appended_path "/#{@locale}/#{@content}"
    else
      redirect_to 'http://www.centurycouncil.org/'
    end
  end
end
