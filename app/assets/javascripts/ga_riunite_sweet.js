$(function() {

  // Site Section Links
  $('.menu').find('a[href="#sweet-red"]').click(function() {
    _gaq.push(['_trackPageview','/sweet-red']);
  });

  $('.menu').find('a[href="#sweet-white"]').click(function() {
    _gaq.push(['_trackPageview','/sweet-white']);
  });

  $('.menu').find('a[href="#video"]').click(function() {
    _gaq.push(['_trackPageview','/video']);
  });

  $('.menu').find('a[href="http://locator.banfi-hq.com/WineLocator/Default.aspx?BCode=289"]').click(function() {
    _gaq.push(['_trackPageview','/store-locator']);
  });

  $('.menu').find('a[href="#"]').click(function() {
    _gaq.push(['_trackEvent','facebook','our-wines', 'link-to-timeline']);
  });

  $('.menu').find('a[href="#chill-chatter-content"]').click(function() {
    _gaq.push(['_trackPageview','/fb-chill-chatter']);
  });

  //Facebook iphone tab link tracking
  $('a[href="https://www.facebook.com/RiuniteSweet/app_467962169882912"]').click(function () {
    _gaq.push(['_trackPageview','/free-iphone']);
  });

  //Get it link tracking
  $('#get-it-button').click(function () {
    _gaq.push(['_trackEvent','main','get-it', 'link-to-iphone-fb']);
  });

  $('#iphone-fb').click(function() {
    _gaq.push(['_trackPageview','/free-iphone']);
  });

  $('a[href="https://www.facebook.com/riunitesweet"]').click(function () {
    _gaq.push(['_trackSocial','facebook','big-visit',]);
  });

  $('a[href="https://www.facebook.com/riunitesweetlatino"]').click(function () {
    _gaq.push(['_trackSocial','facebook','big-visit-es',]);
  });

  //
  //JQuery Serialize Object Plugin
  //
  (function($,undefined){
    '$:nomunge'; // Used by YUI compressor.

    $.fn.serializeObject = function(){
      var obj = {};

      $.each( this.serializeArray(), function(i,o){
        var n = o.name,
    v = o.value;

    obj[n] = obj[n] === undefined ? v
      : $.isArray( obj[n] ) ? obj[n].concat( v )
      : [ obj[n], v ];
      });

      return obj;
    };

  })(jQuery);

  //age verification form tracking code
  $('#age-verify-form').submit(function (e) {

    var age_verify_form = $(this);
    var form_values = age_verify_form.serializeObject();

    var form_values_object = {
      year  : form_values.year
    };

    var form_values_string = JSON.stringify(form_values_object);

    _gaq.push(['_trackEvent', 'age-verify', 'submit', form_values_string]);
    return true;
  });

  //
  //Chill Chatter questions tracking code
  //
  //next question tracking
  $('#next-question').click(function () {
    question_text = $('.question').text();
    _gaq.push(['_trackEvent','main','next-question', question_text]);
  });

  //share question tracking
  $('#share-question').click(function () {
    question_text = $('.question').text();
    _gaq.push(['_trackEvent','main','share-question', question_text]);
  });

  $('.connect_widget_like_button').click(function () {
    _gaq.push(['_trackSocial','facebook','like',]);
  });

  $('#twitter-content.a').click(function () {
    _gaq.push(['_trackSocial','twitter','tweet',]);
  });

  //
  //Social Exit tracking
  //
  $('.facebook').click(function () {
    _gaq.push(['_trackSocial','facebook','visit',]);
  });

  $('.twitter').click(function () {
    _gaq.push(['_trackSocial','twitter','visit',]);
  });

  $('.youtube').click(function () {
    _gaq.push(['_trackSocial','youtube','visit',]);
  });

  $('.pinterest').click(function () {
    _gaq.push(['_trackSocial','pinterest','visit',]);
  });

  $('.spotify').click(function () {
    _gaq.push(['_trackSocial','spotify','visit',]);
  });

  // Videos
  $('#apartment-trigger').click(function () {
    _gaq.push(['_trackEvent','main','chill-girl-videos', 'apartment']);
  });

  $('#rooftop-trigger').click(function () {
    _gaq.push(['_trackEvent','main','chill-girl-videos', 'rooftop']);
  });

  $('#apartment-spanish-trigger').click(function () {
    _gaq.push(['_trackEvent','main','chill-girl-videos', 'apartment-es']);
  });

  //How to play button tracking
  $('#how-to-play').click(function () {
    question_text = $('.question').text();
    _gaq.push(
      ['_trackPageview','where-chill-in-person'],
      ['_trackEvent','main','how-to-play', question_text]
    );
  });

  //How to play pagination tracking
  $('#in-person-trigger').click(function () {
    _gaq.push(['trackPageview','where-chill-in-person']);
  });

  $('#online-trigger').click(function () {
    _gaq.push(['trackPageview','where-chill-online']);
  });

  //Sweet White and Sweet Red Spotify tracking
  $('.porch').click(function () {
    _gaq.push(['_trackEvent','spotify','sweet-red', 'porch-rockin']);
  });

  $('.after-hours').click(function () {
    _gaq.push(['_trackEvent','spotify','sweet-red', 'after-hours']);
  });

  $('.bbq').click(function () {
    _gaq.push(['_trackEvent','spotify','sweet-red', 'backyard-bbq']);
  });

  $('.coffee').click(function () {
    _gaq.push(['_trackEvent','spotify','sweet-red', 'coffe-table']);
  });

  $('.unplugged').click(function () {
    _gaq.push(['_trackEvent','spotify','sweet-red', 'unplugged']);
  });

  $('.water').click(function () {
    _gaq.push(['_trackEvent','spotify','sweet-red', 'on-the-water']);
  });

  $('.summer').click(function () {
    _gaq.push(['_trackEvent','spotify','sweet-red', 'south-summer']);
  });

  $('.party').click(function () {
    _gaq.push(['_trackEvent','spotify','sweet-red', 'party-prep']);
  });

  $('.chatter').click(function () {
    _gaq.push(['_trackEvent','spotify','sweet-red', 'random']);
  });

  //MaxPoint Tracking
  //
  //MaxPoint Chill Chatter tracking
  $('#how-to-play').click( function() {
    $('body').append('<img src=" http://mpp.mxptint.net/1/1143/4/cchowtoplay/?rnd=' + parseInt(9999999999*Math.random()) + '" width="1" height="1" border="0" alt="" />');
  });

  $('#next-question').click( function() {
    $('body').append('<img src=" http://mpp.mxptint.net/1/1143/4/ccnextquestion/?rnd=' + parseInt(9999999999*Math.random()) + '" width="1" height="1" border="0" alt="" />');
  });

  $('#share-question').click( function() {
    $('body').append('<img src=" http://mpp.mxptint.net/1/1143/4/ccsharequestion/?rnd=' + parseInt(9999999999*Math.random()) + '" width="1" height="1" border="0" alt="" />');
  });

  //MaxPoint Iphone Facebook Registration tracking
  $('a[href="https://www.facebook.com/RiuniteSweet/app_467962169882912"]').click(function () {
    $('body').append('<img src=" http://mpp.mxptint.net/1/1143/4/registrationfblink/?rnd=' + parseInt(9999999999*Math.random()) + '" width="1" height="1" border="0" alt="" />');
  });
});