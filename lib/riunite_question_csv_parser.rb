# encoding: utf-8
require 'csv'
# coding: utf-8
# Name of the CSV file to parse
# Note: This assumes the CSV format is valid and that the question text is in the first column.
# This should be updated once the spanish text is included.
filename = "questions_en_es.csv"

# Output the text to paste into the seed.db file
CSV.foreach(filename) do |row|
	row[0].to_s.gsub!(/[”“]/, '"')
	row[0].to_s.gsub!(/[‘’]/, "\'")
	row[0].to_s.gsub!('–' , '-')
	row[0].to_s.gsub!(/["]/ , "\'")
	puts "Question.create(en: \"#{row[0]}\", es: \"#{row[1]}\");"
end

# Copy the output from the terminal and paste it into db/seeds.rb.
# !!! SAVE seeds.rb AS UTF-8 with BOM or the spanish chars won't load into the database. !!!