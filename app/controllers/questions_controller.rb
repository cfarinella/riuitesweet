class QuestionsController < ApplicationController

  # GET /:locale/questions.json
  def index
    #language_question_index = 'questions.' + params[:locale].to_s
    @questions = Question.find(:all).map {
      |question| {
        question.id =>
        (params[:locale].to_s == 'en') ? question.en : question.es
      }
    }

    respond_to do |format|
      format.json { render json: @questions }
    end
  end
end