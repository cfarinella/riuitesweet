class MainController < AgegatedController

  def index
    if(params[:content] == 'sweet-red-wine' || params[:content] == 'sweet-white-wine' || params[:content] == 'chill-girls-videos')
      @content = params[:content]
    else
      @meta_id = params[:content] || ''
      @meta_description = ''
      if @locale.to_s == "es"
        begin
          @meta_description = Question.find(@meta_id).es
          rescue ActiveRecord::RecordNotFound
        end
      else
        begin
          @meta_description = Question.find(@meta_id).en
          rescue ActiveRecord::RecordNotFound
        end
      end
      
    end
  end

  

end