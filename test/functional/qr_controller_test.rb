require 'test_helper'

class QrControllerTest < ActionController::TestCase

  setup do
    @test_paths = {
      'qrc12-00000001' => 'http://www.riuniteice.com/en/?cid=qrc12-00000001',
      'qrc12-00000002' => 'http://www.riunitevino.com/es/?cid=qrc12-00000001',
      'qrc12-00000003' => 'http://www.riunitechill.com/en/?cid=qrc12-00000003',
      'qrc12-00000004' => 'http://www.riunitedulce.com/es/?cid=qrc12-00000004'
    }
  end

  test "should go to expected path for qr code" do
    @test_paths.each do  |cid, url|
      get :index, {'cid' => cid}

      assert_redirected_to url
    end
  end
  
end