// https://developers.google.com/youtube/iframe_api_reference

// This function creates an <iframe> (and YouTube player) after the API code downloads.
var player;
var videoId;
function onYouTubePlayerAPIReady() {
  player = new YT.Player('video-container', {
    height: '360',
    width: '640',
    videoId: videoId,
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}

// The API will call this function when the video player is ready.
function onPlayerReady(event) {
  event.target.playVideo();
}

// The API calls this function when the player's state changes.
// The function indicates that when playing a video (state=1),
// the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
}

function stopVideo() {
  player.stopVideo();
}
