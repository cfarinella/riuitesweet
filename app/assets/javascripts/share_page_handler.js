SharePageHandler = {

	currentQuestion: null,	
	locale: null,
	currentHost: null,

	setCurrentQuestion: function(questionObject, host, locale) {
		this.currentQuestion = questionObject;
		this.locale = locale;
		this.currentHost = host;
	},

	getCurrentQuestion: function() {
		return this.currentQuestion;
	},

	createShareWindow: function() {
		var params = this.getParams();
		if (this.currentQuestion.id != 0) {
			window.open('http://'+this.currentHost+'/'+this.locale+'/share/'+params,'','width=492,height=244,left=350,top=200,location=no,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
		}
	},

	getParams: function() {
		return this.currentQuestion.id; //+ "/" + encodeURIComponent(this.currentQuestion.text);
	}

}