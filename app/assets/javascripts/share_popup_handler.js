SharePopupHandler = {

	currentQuestion: null,	
	locale: null,
	currentHost: null,
	sharePopup: null,

	setCurrentQuestion: function(questionObject, host, locale, elem) {
		this.currentQuestion = questionObject;
		this.locale = locale;
		this.currentHost = host;
		this.sharePopup = $(elem);
	},

	getCurrentQuestion: function() {
		return this.currentQuestion;
	},

	createShareWindow: function() {
		var fbLink = 'http://riunitesweet.herokuapp.com/' + this.locale + '/' + this.currentQuestion.id;
		if (this.currentQuestion.id != 0) {
			this.sharePopup.find('#question-text').html(this.currentQuestion.text);
			this.sharePopup.find('.fb-like').attr({'data-href': fbLink });
			this.sharePopup.show();
		}
	},

	closeShareWindow: function() {
		this.sharePopup.hide();
	},

	getParams: function() {
		return this.currentQuestion.id; //+ "/" + encodeURIComponent(this.currentQuestion.text);
	}

}