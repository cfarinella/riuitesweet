function ContentHandler() {

	$elem = null;
	$tab_menu = null;
	$tab_contents = null;
	var background_image = null;
	var href_attr = null;

	this.init = function(tab_menu, tab_contents, env, contentParam){
		$tab_menu   	= $(tab_menu);
		$tab_contents 	= $(tab_contents);
		background_image = 'url(' + env + '/assets/layout/tab-active.png)';
		
		if(contentParam == 'sweet-red-wine') {
			$elem = $('#sweet-red');
			href_attr = '#sweet-red';
		}
		else if(contentParam == 'sweet-white-wine') {
			$elem = $('#sweet-white');
			href_attr = '#sweet-white';
		}
		else if(contentParam == 'chill-girls-videos') {
			$elem = $('#video');
			href_attr = '#video';
		}
		if($elem != null) {
			handle();
		}
		
	}

	function handle() {
		
		// hide the active state on everything
		$tab_menu.find('.caption-wrapper').each(function() {	
			$(this).removeClass('caption-wrapper-active');
		});
		
		// add it back in to the appropriate one
		$tab_menu.find('a').each(function() {
			var href = $(this).attr('href');
			if(href == href_attr) {
				$(this).find('.caption-wrapper').addClass('caption-wrapper-active');
			}
		});

		$tab_contents.find('.show-tab-content').each(function() {
			$(this).hide();
		});
		$elem.show();
		$('html, body').animate({scrollTop: $('.content').height()}, 500);
	}
}