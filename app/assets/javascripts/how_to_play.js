HowToPlay = {
	button: null,
	close: null,
	online: null,
	in_person: null,

	init: function(button, close, online, in_person) {
		this.button = $(button);
		this.close = $(close);
		this.online = $(online);
		this.in_person = $(in_person);

		this.bindActions();
	},

	bindActions: function() {
		var parent = this;
		this.button.on('click', function(event){
			event.preventDefault();
			parent.showHideWindow();
		});
		this.close.on('click', function(event){
			event.preventDefault();
			parent.showHideWindow();
		});
		this.online.on('click', function(event){
			event.preventDefault();
			parent.onlineClickHandler();
		});
		this.in_person.on('click', function(event){
			event.preventDefault();
			parent.inPersonClickHandler();
		});
	},

	showHideWindow: function() {
		var popWindow = $('#how-to-play-window');
		if (popWindow.is(':visible')) {
			popWindow.hide();
		}
		else {
			popWindow.show();
		}
	},

	onlineClickHandler: function() {
		$('#in-person').hide();
		$('#online').show();
	},

	inPersonClickHandler: function() {
		$('#online').hide();
		$('#in-person').show();
	}
}