class QrController < ApplicationController
  def index
    @path_lookups = {
      # 'qrc12-00000001' => 'http://www.riuniteice.com/en/?cid=qrc12-00000001',
      # 'qrc12-00000002' => 'http://www.riunitevino.com/es/?cid=qrc12-00000002',
      # 'qrc12-00000003' => 'http://www.riunitechill.com/en/?cid=qrc12-00000003',
      # 'qrc12-00000004' => 'http://www.riunitedulce.com/es/?cid=qrc12-00000004'

      'qrc12-0000001' => 'http://www.riunitesweet.com/en/?cid=qrc12-0000001',
      'qrc12-0000002' => 'http://www.riunitesweet.com/es/?cid=qrc12-0000002',
      'qrc12-0000003' => 'http://www.riunitesweet.com/en/?cid=qrc12-0000003',
      'qrc12-0000004' => 'http://www.riunitesweet.com/es/?cid=qrc12-0000004'
    }

    params[:cid] = params[:cid] || 'qrc12-00000001';

    if(params[:cid]) then
      redirect_to @path_lookups[params[:cid]]
    else
      redirect_to 'http://www.riunitesweet.com'
    end
  end
end