
class ShareController < ApplicationController

	def index
  	@meta_image = "http://#{request.env["HTTP_HOST"]}/assets/share_question.png"

  	# handle facebook request for meta tags (allows question text
    # to be embedded in description)
    @meta_id = params[:id] || ''
    @meta_description = ''
    if @locale.to_s == "es"
      begin
        @meta_description = Question.find(@meta_id).es
        rescue ActiveRecord::RecordNotFound
      end
    else
      begin
        @meta_description = Question.find(@meta_id).en
        rescue ActiveRecord::RecordNotFound
      end
    end

  	render :layout => "share-layout"
  end

end
