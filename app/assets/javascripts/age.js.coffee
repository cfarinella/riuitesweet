$ -> 
	$('#age-verify-form').submit () ->
		
		month = $(this).find('input[name=month]').val()
		day   = $(this).find('input[name=day]').val()
		year  = $(this).find('input[name=year]').val()

		date = month+'/'+day+'/'+year	
			
		if !date.match /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/
			$('#invalid-date-error').fadeIn()
			return false
	
		return true;