class AgegatedController < ApplicationController
  before_filter :fb_redirect, :verified_age

  def fb_redirect

    # I18n.locale = params[:locale] || I18n.default_locale
    # @locale = I18n.locale
    # params[:locale] = @locale
    
    # #Permanent redirect to facebook.
    # if ENV['RAILS_ENV'] == "production"
    #   if @locale.to_s === "en"
    #     redirect_to 'http://www.facebook.com/RiuniteSweet', :status => 301
    #   else
    #     redirect_to 'http://www.facebook.com/RiuniteSweetLatino', :status => 301
    #   end
    # end
  end


  def verified_age
    # user agent shiz
    request_user_agent = request.env["HTTP_USER_AGENT"]
    user_agent_robot = Useragent.find_by_name(request_user_agent)

    # content shiz
    @content = ''
    if params[:content] == 'sweet-red-wine' || params[:content] == 'sweet-white-wine' || params[:content] == 'chill-girls-videos'
      @content = params[:content]
    end

    unless session[:age_verified] == 1 || user_agent_robot != nil
      #if @content != ''
       # redirect_to "/#{params[:locale]}/age" + extract_google_adword_params(), :status => 301
      #else
        redirect_to "http://#{request.env["HTTP_HOST"]}/#{params[:locale]}/age" + extract_google_adword_params(), :status => 301
      #end
    end
  end

  private

  def extract_google_adword_params
    params_to_exclude = ['controller', 'action', 'locale']
    google_params = ""
    extracted_param_count = 0
    params.each do |p|
      if (p.count == 2)
        if !params_to_exclude.include?(p[0])
          if (extracted_param_count > 0)
            google_params += "&"
          end
          google_params += (p[0].to_s + "=" + p[1].to_s)
          extracted_param_count += 1
        end
      end
    end
    if google_params.length > 0
      google_params = "?" + google_params
    end
    google_params
  end
end
