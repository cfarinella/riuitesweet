RandomQuestionIterator = {

	questionsArray: null,
	questionsRemainingArray: null,
	offer: null,
	counter: 0,

	init: function(questions_array, offer){
		this.questionsArray = this.setQuestionsArray(questions_array);
		this.questionsRemainingArray = this.questionsArray.slice(0);
		this.offer = offer;
	},

	setQuestionsArray: function(responseArray) {
		var retVal = new Array();
		for(var i = 0; i < responseArray.length; i++) {
			for(var key in responseArray[i]) {
				if(responseArray[i].hasOwnProperty(key)) {
					var questionObject = {
											"id": key,
											"text": responseArray[i][key]
										};
					retVal[i] = questionObject;
				}
			}
		}
		return retVal;
	},

	getNextQuestionAndRemoveFromRemaining: function() {
		var question;

		// If all of the questions have been seen then refresh the list
		if(this.questionsRemainingArray.length <= 0) {
			this.questionsRemainingArray = this.questionsArray.slice(0);
		}

		// Grab a random question from a list of those yet to be used
		var i = Math.floor((Math.random() * this.questionsRemainingArray.length));
		question = this.questionsRemainingArray[i];

		// Remove that question from the array
		this.questionsRemainingArray.splice(i,1);
		return question;
	},

	getActiveRewardMessage: function() {

		return {
				"id": 0,
				"text": this.offer
				};
	},

	next: function() {
		var retVal = "";
		//Change to 5 when the reward offer is valid, otherwise -1
		if(this.counter == -1) {
			retVal = this.getActiveRewardMessage();
		}
		else {
			retVal = this.getNextQuestionAndRemoveFromRemaining();
		}
		this.counter++;
    return retVal;
	}

}