VideoHandler = {
  apartmentTrigger: null,
  apartmentId: 'usD0N0VVJ34',

  rooftopTrigger: null,
  rooftopId: 'SWeBmCBC4rk',

  latinoTrigger: null,
  latinoId: '27h1pZsWbKI',

  closeTrigger: null,

  init: function(apartment, rooftop, latino, close) {
    this.apartmentTrigger = $(apartment);
    this.rooftopTrigger = $(rooftop);
    this.latinoTrigger = $(latino);
    this.closeTrigger = $(close);

    this.player = player;

    this.bindActions();
  },

  bindActions: function() {
    var parent = this;

    this.apartmentTrigger.on('click', function(event){
      event.preventDefault();
      parent.playVideo(parent.apartmentId);
    });

    this.rooftopTrigger.on('click', function(event){
      event.preventDefault();
      parent.playVideo(parent.rooftopId);
    });

    this.latinoTrigger.on('click', function(event){
      event.preventDefault();
      parent.playVideo(parent.latinoId);
    });

    this.closeTrigger.on('click', function(event){
      event.preventDefault();
      parent.closeButtonClick();
    });
  },

  playVideo: function(id) {
      videoId = id;
      // This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');
      tag.src = "http://www.youtube.com/player_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      $('#video-popup').show();

      if(player) {
        player.loadVideoById(id);
      }
  },

	closeButtonClick: function() {
    if(player) {
      player.stopVideo();
    }
		$('#video-popup').hide();
	}
}