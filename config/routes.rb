RinuiteSweet::Application.routes.draw do
  namespace :admin do
    resources :questions
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  match '/age'              => 'age#index'
  match '/age/:id'          => 'age#index'
  match '/age/verify'       => 'age#verify'

  match 'content/:content'  => 'main#index'


  match '/qr'               => 'qr#index'

  match '/share/:id/:text'  => 'share#index'

  scope "/:locale" do
    #match ':controller(/:action(/:id))(.:format)'
    match '/'                     => 'main#index'
    match '/age'                  => 'age#index'
    match '/age/verify'           => 'age#verify'
    match '/age/verify/:content'  => 'age#verify'
    match '/age/:id'              => 'age#index'
    match '/questions.json'       => 'questions#index'
    match '/main/share/:id'       => 'main#share'
    match '/iphone'               => 'facebook#giveaway'
    match '/party-pack'           => 'facebook#partypack'
    match '/corkscrew'            => 'facebook#corkscrew'
    match '/facebook'             => 'facebook#index'
    match '/share/:id'            => 'share#index'
    #match '/red'                 => 'iframe#red'
    #match '/white'               => 'iframe#white'
    match '/:content'   => 'main#index'

  end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.

  # Redirect US sites to www.<host>.com
  constraints(:host => "riuniteice.com") do
    match '/', :to => redirect {|params, req| "http://www.riuniteice.com/en#{params[:path]}"}
  end
  constraints(:host => "riunitechill.com") do
    match '/', :to => redirect {|params, req| "http://www.riunitechill.com/en#{params[:path]}"}
  end

  # Redirect spanish specific sites to spanish locale path
  constraints(:host => "riunitedulce.com") do
    match '/', :to => redirect {|params, req| "http://www.riunitedulce.com/es#{params[:path]}"}
  end
  constraints(:host => "riunitevino.com") do
    match '/', :to => redirect {|params, req| "http://www.riunitevino.com/es#{params[:path]}"}
  end
  constraints(:host => "www.riunitedulce.com") do
    match '/', :to => redirect {|params, req| "http://www.riunitedulce.com/es#{params[:path]}"}
  end
  constraints(:host => "www.riunitevino.com") do
    match '/', :to => redirect {|params, req| "http://www.riunitevino.com/es#{params[:path]}"}
  end

  root :to => redirect('/en');
end